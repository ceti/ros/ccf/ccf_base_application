/*! \file demo.cpp

    A ROS node that simulates a node connected to a model-based cobotic application

    \author Johannes Mey
    \author Sebastian Ebert
    \date 01.09.2021
*/

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // fix boost

#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Empty.h>

#include "ccf/controller/DummyRobotArmController.h"
#include "ccf/connection/NngConnection.h"
#include "ccf/util/NodeUtil.h"

std::string NODE_NAME = "demo";

using CetiRosToolbox::getParameter;
using CetiRosToolbox::getPrivateParameter;

int main(int argc, char **argv) {

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    ros::init(argc, argv, NODE_NAME);
    NODE_NAME = ros::this_node::getName();

    ros::NodeHandle n("connector_node_ros_ccf"); // namespace where the connection_address is

    auto connectionAddress = getPrivateParameter<std::string>("connection_address", "tcp://*:6576");

    DummyRobotArmController connector{n, NODE_NAME};

    // add an NNG connection
    std::unique_ptr<NngConnection> connection = std::make_unique<NngConnection>(connectionAddress);
    connection->setReceiveTopic("nng_incoming_messages");
    connection->setSendTopic("outgoing_messages");
    connector.addConnection(std::move(connection));


    // load a scene from disk.
    connector.loadScene(getPrivateParameter<std::string>("scene", ros::package::getPath("ccf_immersive_sorting") +
                                                                  "/config/config_scene.yaml"));

    ros::Subscriber sub = n.subscribe<std_msgs::Empty>("someTopic", 1000, [&connector](
            const std_msgs::EmptyConstPtr &msg) {
        // do something whenever a message is received on this topic
    });

    ros::Timer timer = n.createTimer(ros::Duration(10), [&connector](const ros::TimerEvent &event) {
        // do something every 10 seconds
    });

    auto selectionMessageCallback = [&connector, &n](const Selection &selection) {

        // use a skill

        // if (!connector.pickAndDrop(*robot, *selectedBox, *selectedBin, false)) {
        //     ROS_WARN_STREAM("[" << NODE_NAME << "] Unable to remove box '" << boxId << "'!");
        // }

    };
    connector.reactToSelectionMessage(selectionMessageCallback);

    auto sceneUpdateCallback = [&connector]() {
        // do something
    };
    connector.reactToSceneUpdateMessage(sceneUpdateCallback);

    ros::spin();

    return 0;
}